import React from 'react';
import block from 'bem-cn';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { addNotify } from 'features/notify';
import { SlideOutModel } from 'shared/models/SlideOutModel';


import Notify from 'features/notify/mobile';
import SlideOut from 'components/SlideOut';

import './App.scss';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.appRef = React.createRef();
  }

  state = {
    isMenuOpen: false,
  }

  static propTypes = {
    lang: PropTypes.string.isRequired,
    locale: PropTypes.object.isRequired,
    children: PropTypes.node.isRequired,
  }

  componentDidMount() {
    const touchListener = new SlideOutModel(value => this.setState({ isMenuOpen: value }), this.appRef.current);
    touchListener.init();
  }

  componentWillReceiveProps(nextProps) {
    const { location } = nextProps;

    if (location.pathname !== this.props.location.pathname) {
      this._scrollToTop();
    }
   }

  render() {
    const b = block('app');
    const { locale, children } = this.props;
    const { isMenuOpen } = this.state;
    return (
      <div className={b()} ref={this.appRef}>
        <Notify />
        <SlideOut isOpen={isMenuOpen}>
          <div className={b('scroll-block')}>
            {children}
          </div>
        </SlideOut>
      </div>
    );
  }



  _scrollToTop = () => {
    const el = document.querySelector('.app__scroll-block');
    el.scrollTop = 0;
  };

}

function mapStateToProps(state) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  const actions = {
    addNotify,
  };
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)((withRouter(React.memo(App))));
