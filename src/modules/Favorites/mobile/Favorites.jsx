import React from 'react';
import { Route } from 'react-router-dom';

import { FavoritesLayout } from './view/FavoritesLayout';

export class FavoritesModule {
  getRoutes() {
    return <Route key="/favorites" path="/favorites" component={FavoritesLayout} />;
  }
}