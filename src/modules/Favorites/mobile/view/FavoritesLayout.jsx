import React from 'react';
import block from 'bem-cn';

import Favorites from 'features/favorites/mobile';

export const FavoritesLayout = () => {
  const b = block('favorites-layout');
  return (
    <div className={b()}>
      <Favorites />
    </div>
  );
};