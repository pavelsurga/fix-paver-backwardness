import React from 'react';
import block from 'bem-cn';
import PropTypes from 'prop-types';

import './SlideOut.scss';

const SlideOut = ({ children, menu, isOpen }) => {
  const b = block('slide-out');
  return (
    <div className={b({ open: isOpen })}>
      <div className={b('menu')}>{menu}</div>
      <div className={b('content')}>{children}</div>
    </div>
  );
};

SlideOut.propTypes = {
  children: PropTypes.array.isRequired,
  menu: PropTypes.element.isRequired,

  isOpen: PropTypes.bool.isRequired,
};

export default SlideOut;
