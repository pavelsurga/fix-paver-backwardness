import React from 'react';
import block from 'bem-cn';
import PropTypes from 'prop-types';

import Settings from 'features/settings/mobile';
import { SideMenuItem } from './SideMenuItem/SideMenuItem';
import './SideMenu.scss';

export const SideMenu = ({ menuItems, locale }) => {
  const b = block('side-menu');
  const items = menuItems.map(item => <SideMenuItem key={item.textIdent} item={item} locale={locale} />);
  return <nav className={b()}>
    <ul className={b('list')}>
      {items}
      <Settings />
    </ul>
  </nav>;
};

SideMenu.propTypes = {
  menuItems: PropTypes.array.isRequired,
  locale: PropTypes.object,
};
