import React, { useState } from 'react';
import block from 'bem-cn';
import PropTypes from 'prop-types';
import SVGInline from 'react-svg-inline';
import { Link } from 'react-router-dom';

import ArrowSVG from './img/arrow.svg';
import './SideMenuItem.scss';

export const SideMenuItem = ({ item, locale }) => {
  const b = block('side-menu-item');
  const [isOpen, changeOpen] = useState(true);

  const inItems = item.childs ? item.childs.map(inItem => <li className={b('in-item')} key={inItem.textIdent}>
    <div className={b('in-item-circle')} />
    <Link className={b('in-item-link')} to={inItem.link}>
      {locale[inItem.textIdent]}
    </Link>
  </li>) : null;

  return <li className={b({ open: isOpen })}>
    <div className={b('main')}>
      <Link className={b('link')} to={item.link}>
        <SVGInline className={b('icon').toString()} svg={item.icon} />
        <span className={b('text')}>{locale[item.textIdent]}</span>
      </Link>
      {item.childs ? <SVGInline
        className={b('arrow').toString()}
        svg={ArrowSVG}
        onClick={() => changeOpen(!isOpen)} /> : null}
    </div>
    {isOpen && <ul className={b('in-items')}>{inItems}</ul>}
  </li>;
};

SideMenuItem.propTypes = {
  item: PropTypes.object.isRequired,
  locale: PropTypes.object,
};
