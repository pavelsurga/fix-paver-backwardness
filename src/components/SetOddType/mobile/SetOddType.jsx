import React from 'react';
import block from 'bem-cn';
import PropTypes from 'prop-types';

import { oddsTypes } from 'features/userSettings/redux/data/odds';

import Modal from 'components/Modal/mobile';

import './SetOddType.scss';

const SetOddType = ({ closeFunction, locale, oddType, setOddType }) => {
  const b = block('set-odd-type');
  const odds = Object.keys(oddsTypes).map(key => {
    const odd = oddsTypes[key];
    const isActive = oddType === odd.name;
    return (
      <li
        key={odd.id}
        className={b('odd', { active: isActive })}
        onClick={isActive ? null : () => {
          setOddType(odd.name);
          closeFunction();
        }}>
        {locale[odd.name]}
      </li>
    );
  });
  return (
    <div className={b('modal').mix('scrollable')}>
      <Modal closeFunction={closeFunction}>
        <div className={b()}>
          <h4 className={b('title')}>{`${locale.coef}:`}</h4>
          <ul className={b('odds-list')}>{odds}</ul>
        </div>
      </Modal>
    </div>
  );
};

SetOddType.propTypes = {
  locale: PropTypes.object.isRequired,
  oddType: PropTypes.string.isRequired,
  closeFunction: PropTypes.func.isRequired,
  setOddType: PropTypes.func.isRequired,
};

export default SetOddType;
