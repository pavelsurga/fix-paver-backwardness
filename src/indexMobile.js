import '@babel/polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import * as OfflinePluginRuntime from 'offline-plugin/runtime';

import configureStore from './configureStore';

import App from './modules/App/mobile';
import { ResultsModule, FavoritesModule } from './modules/mobile';
import './shared/style/mobile.scss';

import Api from './shared/api';
import { createRoutes } from './createRoutes';

OfflinePluginRuntime.install();

const apiUrl = process.env.NODE_ENV === 'production' ? '' : 'https://seven-bet.com';
const api = new Api(apiUrl);

const modules = [
  new ResultsModule(),
  new FavoritesModule(), 
];
const childrens = createRoutes(modules);

const store = configureStore({ api });

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App>
        {childrens}
      </App>
    </BrowserRouter>
  </Provider>, document.getElementById('root'),
);
