
import ResultsApi from './ResultsApi';

class Api {
  constructor(baseUrl = '') {
    this.results = new ResultsApi(baseUrl);
  }
}

export default Api;
