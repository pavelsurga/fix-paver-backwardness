import dayjs from 'dayjs';

export class ResultsConverter {
  
  convertSports(response) {
    const convertedResponce = response.map(item => ({
      value: item.Id,
      name: item.Name,
    }));
    return convertedResponce;
  }

  convertTournaments(response) {
    let convertedResponce = [];
    response.forEach(country => {
      convertedResponce.push({ value: `country${country.ID}`, name: country.Name, countryID: country.ID });
      country.Child.forEach(tournament => convertedResponce.push({ value: `tournament${tournament.ID}`, name: tournament.Name }));
    });
    return convertedResponce;
  }

  convertEvent(response) {
    const convertedResponce = {
      date: dayjs(+(response.Date.match(/\d/g).join(''))).format('DD.MM.YYYY'),
      time: dayjs((+(response.Date.match(/\d/g).join('')))).format('HH:mm'),
      ID: response.ID,
      teamName1: response.NameTeam1,
      teamName2: response.NameTeam2,
      score: response.Score,
    };
    return convertedResponce;
  }

  convertResults = response => {
    let convertedResponce = {};
    response.forEach(tourney => {
      const tourneyID = tourney.TournamentId;
      const event = tourney.DateInfos[0].Events[0];
      convertedResponce[tourneyID] = {
        name: tourney.Name,
        sportID: tourney.SportId,
        ID: tourneyID,
        countryID: tourney.countryID,
        events: convertedResponce[tourneyID] ? [...convertedResponce[tourneyID].events, this.convertEvent(event)] : [this.convertEvent(event)],
      };
    });
    return convertedResponce;
  }

}