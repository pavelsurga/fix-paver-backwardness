import dayjs from 'dayjs';

import BaseApi from '../BaseApi';
import { ResultsConverter } from './ResultsConverter';

class ResultsApi extends BaseApi {
  constructor(baseUrl) {
    super(baseUrl);
    this.baseUrl = `${baseUrl}/api/bs2/remote/api/results`;
    this.converter = new ResultsConverter();
  }

  getSports = (data, lang='ru-ru') => {
    const convertedBegin = dayjs(data.beginDate).format('DDMMYYYY');
    const convertedEnd = dayjs(data.endDate).format('DDMMYYYY');
    return this.sendLineQuery(
      this.queryTypes.GET,
      `${this.baseUrl}/${lang}/sports/0/${convertedBegin}/${convertedEnd}`,
      null, null,
      this.converter.convertSports,
    );
  }

  getTournaments = (data, lang='ru-ru') => {
    const convertedBegin = dayjs(data.beginDate).format('DDMMYYYY');
    const convertedEnd = dayjs(data.endDate).format('DDMMYYYY');
    return this.sendLineQuery(
      this.queryTypes.GET,
      `${this.baseUrl}/${lang}/tournaments/sport${data.sportID}/0/${convertedBegin}/${convertedEnd}`,
      null, null,
      this.converter.convertTournaments,
    );
  }

  getResults = (data, lang='ru-ru') => {
    const convertedBegin = dayjs(data.beginDate).format('DDMMYYYY');
    const convertedEnd = dayjs(data.endDate).format('DDMMYYYY');
    return this.sendLineQuery(
      this.queryTypes.GET,
      `${this.baseUrl}/${lang}/sport${data.sportID}/${data.country}/${data.tournament}/0/${convertedBegin}/${convertedEnd}`,
      null, null,
      this.converter.convertResults,
    );
  }
}

export default ResultsApi;