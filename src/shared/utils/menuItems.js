export const menuItems = [
  {
    link: '/line/top-events',
    textIdent: 'line',
  },
  {
    link: '/live',
    textIdent: 'live',
  },
  {
    link: '/cybersport',
    textIdent: 'cybersport',
  },
  // {
  //   link: '/toto',
  //   textIdent: 'totalizator',
  // },
  {
    link: '/slots/all',
    textIdent: 'slots',
  },
  {
    link: '/casinolive',
    textIdent: 'casinolive',
  },
  {
    link: '/results',
    textIdent: 'results',
  },
  {
    link: '/contacts',
    textIdent: 'support',
  },
];
