export const remouteLanguages = {
  'ru-ru': 'ru',
  'en-US': 'en',
};

export const lang = {
  'en-US': 1,
  'ru-ru': 0,
};

export const getLangIdByName = name => {
  const langId = lang[name];
  return langId;
};