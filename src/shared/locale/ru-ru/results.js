export const results = {
  results: 'Результаты',
  kindOfSport: 'Вид спорта',
  tournament: 'Чемпионат',
  from: 'С',
  to: 'по',
  show: 'Показать',
  result: 'Результат',
  date: 'Дата',
  addSuccess: 'Событие добавлено в избранное',
  deleteSuccess: 'Событие удалено из избранного',
};