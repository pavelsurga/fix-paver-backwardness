export const auth = {
  inputPhone: 'Введите телефон или E-mail',
  inputPassword: 'Введите пароль',
  password: 'Пароль',
  phone: 'Телефон',
  authorization: 'Авторизация',
  registration: 'Регистрация',
  login: 'Войти',
  socialAuthorization: 'Авторизация через соц-сети:',
  byNumberPhone: 'По номеру телефона',
  byEmail: 'По E-mail',
  bySocial: 'Через соц.сеть',
  oneClick: 'В 1 клик',
  email: 'E-mail',
  currencyBets: 'Валюта ставок',
  repeatPassword: 'Повторите пароль',
  iAssign: 'Я подтверждаю, что я ознакомлен и полностью согласен с',
  terms: 'Условиями Соглашения об использовании сайта',
  numberPhone: 'Номер телефона',
  sendCode: 'Отправить код',
  iHaveCode: 'У меня уже есть код',
  code: 'Код из смс',
  needAuth: 'Необходимо авторизоваться',
  successAuth: 'Авторизация прошла успешно',
  successReg: 'Регистрация прошла успешно',
  succesRecovery: 'Смена пароля прошла успешно',
  forgotPassword: 'Забыли пароль?',
  checkEmail: 'Пожалуйста, проверьте ваш email',
  checkPhone: 'Пожалуйста, проверьте ваш телефон',
  dontForgetSave: 'Не забудьте сохранить логин и пароль',
  recoveryFailed: ' Не удалось восстановить пароль',
  copiedSuccess: 'Данные аутентификации успешно скопированы',
  recoverySuccess: 'Пароль успешно воостановлен',
  tryAgain: 'Превышен дневной лимит SMS сообщений ',
  pleaseCheckAuth: 'Пожалуйста проверьте свою почту и потвердите регистрацию',
};
