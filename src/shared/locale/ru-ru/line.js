export const line = {
  sport: 'Спорт',
  loading: 'Загрузка',
  showAll: 'Показать все',
  collapse: 'Свернуть',
  topEvents: 'Топ лиги',
  upcomingEvents: 'Ближайшие события',
  favorites: 'Избранное',
  betNames: [
    'Ставок', 'П1', 'X', 'П2', '1X', '12', '2X', 'М', 'Фора', 'Б', 'М', 'Тотал', 'Б',
  ],
  twoHours: 'До начала менее 2 часов',
  fourHours: 'До начала менее 4 часов',
  sixHours: 'До начала менее 6 часов',
  twelveHours: 'До начала менее 12 часов',
  twentyFourHours: 'До начала менее 24 часа',
  loadingError: 'Ошибка при загрузке',
  cybersport: 'Киберспорт',
};