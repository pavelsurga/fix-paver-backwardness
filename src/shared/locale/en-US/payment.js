export const payment = {
  topUp: 'TopUp account',
  topUpRules: 'Rules of replenishment',
  topUpParagraph1: `To replenish, you must enter the amount, and click the "Replenish" button,
    after which the choice of the payment system will be offered. Currently available
    following systems: QIWI. WALLET, YANDEX.Money, VISA, LITECOIN, BITCOIN, OPAY,
    STEAM PAY, PAYEER, PERFECT MONEY, FK WALLET, ADVCASH, MTS, BEELINE, TELE2, MEGAFON.`,
  topUpParagraph2: 'FREE-KASSA is a payment aggregator of the systems listed above. The minimum amount to replenish 100 RUB (4 $)',
  topUpParagraph3: 'Commission for replenishment is not charged!',
  amount: 'Amount',
  interkassa: 'Interkassa',
  freeKassa: 'Free-Kassa',
  visa: 'VISA',
  qiwi: 'QIWI',
  mastercard: 'Mastercard',
  yandexMoney: 'Yandex Money',
  litecoin: 'Litecoin',
  bitcoin: 'Bitcoin',
  payeer: 'Payeer',
  perfectMoney: 'Perfect Money',
  advcash: 'AdvCash',
  mts: 'МТС',
  beeline: 'Beeline',
  tele2: 'Tele 2',
  megafon: 'MegaFon',
  webmoney: 'WebMoney',
  replenish: 'Replenish',
  withdrawal: 'Withdrawal',
  withdrawalMoney: 'Withdrawal money',
  withdrawalRules: 'Withdrawal rules',
  withdrawalParagraph1: 'To withdraw funds, you must perform the following steps:',
  withdrawalParagraph2: 'The minimum amount to withdrawal 5$.',
  withdrawalItem1: '1. Choose pay system',
  withdrawalItem2: '2. Enter details and amount',
  withdrawalItem3: '3. Press the withdrawal button',
  requisites: 'Requisites',
  payHistory: 'Operations history',
  date: 'Date',
  operationType: 'Operation type',
  status: 'Status',
  withdrawalStatuses: {
    0: 'Created',
    1: 'During',
    2: 'Paid out',
    3: 'Blocked',
    4: 'Canceled',
  },
  paymentStatuses: {
    0: 'During',
    1: 'Canceled',
    2: 'Credited',
  },
  paymentTypes: {
    1: 'Refill balance',
    6: 'Bet win',
    10: 'Withdrawal',
    16: 'Payment with FREEKASSA',
    114: 'Payment with cashier',
  },
  withdrawalTypes: {
    8: 'Withdrawal',
    118: 'Withdrawal with cashier',
  },
  success: 'Success',
  withdrawalSuccess: 'Withdrawal ordered successfully',
  errorPayment: 'Currently only replenishment of ruble accounts is possible',
};