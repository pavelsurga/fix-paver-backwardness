export const live = {
  live: 'Live',
  multiLive: 'Multi-live',
  betNames: [
    'Bets', 'W1', 'X', 'W2', '1X', '12', '2X', 'U', 'Handicap', 'O', 'U', 'Total', 'O',
  ],
  otherSports: 'Other sports',
  allSports: 'All sports',
};
