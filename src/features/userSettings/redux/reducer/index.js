import { initialState } from '../initial';
import { actionTypes } from '../actions';

export function reducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.CHANGE_LANG:
      return {
        ...state,
        lang: action.payload,
      };

    case actionTypes.GET_SETTINGS:
      return {
        ...state,
        ...action.payload,
      };

    case actionTypes.SET_ODD_TYPE:
      return {
        ...state,
        oddType: action.payload,
      };

    default:
      return { ...state };
  }
}