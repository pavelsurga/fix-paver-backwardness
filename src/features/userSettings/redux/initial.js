import { languages } from 'shared/locale';
import { oddsTypes } from './data/odds';

export const initialState = {
  lang: languages.RU,
  currencies: [],
  limits: {
    payment: {},
    bonus: {},
    bets: {},
  },
  oddType: oddsTypes.decimal.name,
};