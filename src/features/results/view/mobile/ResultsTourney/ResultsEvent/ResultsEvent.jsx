import React from 'react';
import PropTypes from 'prop-types';
import block from 'bem-cn';
import SVGInline from 'react-svg-inline';

import favoriteSVG from '../../img/favorite.svg';
import favoriteChoosenSVG from '../../img/yellowStar.svg';
import starSvg from '../../img/star.svg';
import activeStarSvg from '../../img/active-star.svg';

import './ResultsEvent.scss';

const ResultsEvent = ({ event, tournament, favoritesList, addFavorite, removeFavorite, isChosen }) => {
  const b = block('results-event'); 
  isChosen = favoritesList.map((favorite) => { 
    const logic = favorite.ID === event.ID; 
    return logic;
  }).includes(true);
  
  const addToFavorits = (event, tournament) => {
    console.log('add');
    addFavorite(event, tournament);
  };

  const deleteFavorite = (event) => {
    console.log('remove');
    removeFavorite(event);
  };

  return (
    <div className={b()}>
      <div className={b('title')}>
        <div className={b('link')}>
          <div className={b('favorite')}  onClick={() =>{ 
            isChosen ? deleteFavorite(event) : addToFavorits(event,tournament);
            }}>
            <SVGInline className={b('icon').toString()} svg={isChosen ? activeStarSvg : starSvg } />
          </div>
          <div className={b('teams')}>
            <div>{event.teamName1}</div>
            {event.teamName2}
          </div>
          <div className={b('date')}>
            <div className={b('date-date')}>{event.date}</div>
            <div className={b('date-time')}>{event.time}</div>
          </div>
          <div className={b('score')} title={event.score}>{event.score}</div>
        </div>
      </div>
    </div>
  );
};

ResultsEvent.propTypes = {
  event: PropTypes.object.isRequired,
  tournament: PropTypes.string.isRequired,
};

export default ResultsEvent;