import { actionTypes } from '../actions';
import { initialState } from '../initial';

export function reducer(state = initialState, action) {
  switch (action.type) {
          
    case actionTypes.ADD_FAVORITE:
      return { ...state, favoritesList : action.payload };
    
    case actionTypes.REMOVE_FAVORITE: 
      return { ...state, favoritesList : action.payload };

    default:
      return { ...state };
  }
}