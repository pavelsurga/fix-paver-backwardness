import { addNotify } from 'features/notify';

const actionTypes = {
  ADD_FAVORITE: 'favorites/ADD_FAVORITE',
  REMOVE_FAVORITE: 'favorites/REMOVE_FAVORITE',
};

function eventTournamentCombine(event, tournament) {
  const newEvent = { ...event, tournament: tournament };
  return newEvent; 
};

function addFavorite(event, tournament) {
  return async (dispatch, getState, extra) =>{
    const newEvent = eventTournamentCombine(event, tournament);
    const oldList = getState().favorites.favoritesList;
    const newList = oldList.concat(newEvent);
    console.log(newList);
    dispatch({type: actionTypes.ADD_FAVORITE, payload: newList });
  };
}


function removeFavorite(event, locale) {
  return async (dispatch, getState, extra) => {
    const oldList = getState().favorites.favoritesList;
    const deleteFavorite = (event, oldList) => {
      const newList = oldList.filter((item) => item.ID !== event.ID);
      return newList;
    };
    const newList = deleteFavorite(event, oldList);
    console.log(newList);
    dispatch({type: actionTypes.REMOVE_FAVORITE, payload: newList});
  };
}
export {
  actionTypes,
  addFavorite,
  removeFavorite,
};