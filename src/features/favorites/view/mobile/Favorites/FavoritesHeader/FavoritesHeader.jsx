import React from 'react';
import PropTypes from 'prop-types';
import block from 'bem-cn';
import SVGInline from 'react-svg-inline';

import arrowSvg from '../../img/arrow.svg';
import './FavoritesHeader.scss';

const FavoritesHeader = ({ name, isOpen, changeOpen }) => {
  const b = block('favorites-header');
  return (
    <div className={b()} onClick={() => changeOpen(!isOpen)}>
      <div className={b('title')} title={name}>
        <span className={b('text')}>{name}</span>
      </div>
      <div className={b('right')}>
        <div className={b('arrow-box')}>
          <SVGInline className={b('arrow-icon', { closed: !isOpen }).toString()} svg={arrowSvg} />
        </div>
      </div>
    </div>
  );
};

FavoritesHeader.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,

  changeOpen: PropTypes.func.isRequired,
};

export default FavoritesHeader;