import React, { useState, useEffect } from 'react';
import block from 'bem-cn';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import { actions as favoritesActions } from 'features/favorites';
import FavoritesItem from './FavotiteItem/FavoriteItem';
import FavoritesHeader from  './FavoritesHeader/FavoritesHeader';

import './Favorites.scss'

const Favorites = ({ favoritesList, locale, removeFavorite }) => {
    const b = block('favorites');
    const [isOpen, changeOpen] = useState(false);
    const favoritesTable = favoritesList.map(event => 
    <div className={b('favorite-item')}>
        <FavoritesItem
            favoritesList={favoritesList}  
            key={event.ID}
            event={event}
            removeFavorite={removeFavorite}
            tournament={event.tournament} />
    </div>
    );
    return (
        <div className={b()} >
            <Link className={b('link')} to={'/results'}>{'to results'}</Link>
            < FavoritesHeader
                isOpen={true}
                changeOpen={changeOpen}
                name={'Favorite events'}
                locale={locale}
            />
            {<div className={b('favorite-list')}>
                {favoritesTable}
            </div>}
        </div>
    );
};

Favorites.PropTypes = { 
    favoritesList: PropTypes.object.isRequired,
    removeFavorite: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
    return {
      locale: state.locale.results,
      favoritesList: state.favorites.favoritesList,
    };
  }

function mapDispatchToProps(dispatch) {
    const actions = {
        removeFavorite: favoritesActions.removeFavorite,
    };
    return bindActionCreators(actions, dispatch);
  }

  export default connect(mapStateToProps, mapDispatchToProps)(Favorites);