import React from 'react';
import PropTypes from 'prop-types';
import block from 'bem-cn';
import SVGInline from 'react-svg-inline';

import activeStarSvg from '../../img/active-star.svg';

import './FavoriteItem.scss';

const FavoritesItem = ({ event, removeFavorite }) => {
  const b = block('favorite-event'); 

  const deleteFavorite = (event) => {
    console.log('remove');
    removeFavorite(event);
  };

  return (
    <div className={b()}>
      <div className={b('event-tourney')}>
            {event.tournament}
        </div>
      <div className={b('title')}>
        <div className={b('link')}>
            <div className={b('favorite')}  onClick={() =>{ 
                deleteFavorite(event);
                }}>
                <SVGInline className={b('icon').toString()} svg={activeStarSvg} />
            </div>
          <div className={b('teams')}>
            <div>{event.teamName1}</div>
            {event.teamName2}
          </div>
          <div className={b('date')}>
            <div className={b('date-date')}>{event.date}</div>
            <div className={b('date-time')}>{event.time}</div>
          </div>
          <div className={b('score')} title={event.score}>{event.score}</div>
        </div>
      </div>
    </div>
  );
};

FavoritesItem.propTypes = {
  event: PropTypes.object.isRequired,
  tournament: PropTypes.string.isRequired,
};

export default FavoritesItem;