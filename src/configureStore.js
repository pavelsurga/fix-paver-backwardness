import {
  compose,
  applyMiddleware,
  combineReducers,
  createStore,
} from 'redux';
import thunk from 'redux-thunk';
import persistState from 'redux-localstorage';

import { reducer as localeReducer } from './features/locale';
import { reducer as userSettingsReducer } from './features/userSettings';
import { reducer as notifyReducer } from './features/notify';
import { reducer as resultsReducer } from './features/results';
import { reducer as favoritesReducer } from 'features/favorites';


function configureStore(extra) {
  const middlewares = [
    thunk.withExtraArgument(extra),
  ];

  const reducer = createReducer();

  const store = createStore(
    reducer,
    compose(
      applyMiddleware(...middlewares),
      persistState(['favorites']),
      window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__({ trace: true }) : (arg => arg),
    ),
  );

  return store;
}

function createReducer() {
  return combineReducers({
    userSettings: userSettingsReducer,
    notify: notifyReducer,
    locale: localeReducer,
    results: resultsReducer,
    favorites: favoritesReducer,
  });
}


export { createReducer };
export default configureStore;
