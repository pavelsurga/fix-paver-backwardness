# Чем стоит заняться

Общее: 
  
  Не писал регулярных выражений, хотя, штука, наверное, полезная

  Нужно разобраться с методами жизненного цикла react компонентов.
  Сегодня прочитал ,что ComponentWillRecieveProps устарел , что ему на замену пришел getDerivedStateFromProps, не успел с ним разобраться 

  Не успел разобраться с мемоизацией
  Она у нас на проектах используется? 

Таск: "Restore password" 

  Долго разбирался с прокидованием пропсов. Особенно, когда данные прокидываются через два-три компонента, потом трудно разобраться откуда данные пришли
  а откуда не пришли.

  Работал с данными во вьюхе, хотя, позже мне объяснили, что такие вещи нужно делать в экшенах.
  Нужно потренироваться с разделением работы в экшенах и во вьюхе

  Не правильно разделил фичу и компонент 

  Много раз переписывал саму процедуру восстановления пароля. 
  Изначально, не сделал отдельную модалку дял логина, а забирал логин сразу из SignIn
  После наплодил слишком много модалок, после, их осталось только две

  Много раз менял и добавлял нотификации

  Не правильно написал dispatch в экшенах 

  Не отцентрировал надпись под кнопку 

  Слишком сильно нагрузил App открытием модалок и забиранием кода из роута.Псоле, немного разгрузил эти процессы в экшены
  Наверное, нужно изучить как следует ReactRouter 4 

  Ещё нужно разобраться на какие таски стоит отвлекаться, а какие нужно отсрочить. Дофига отвлекался и в итоге часто терял мысль и 
  потом тратил время на решение конфликтов от собственных же коммитов.

  Не разобрался, почему при попытке передать функцию как  callBack  в экшн, она то,срабатывает, то не срабатывает в разных местах

Таск "Модалка после регистрации в один клик"

  Долго разбирался с тем, как правильно вызвать модалку, наверное, стоит потренироваться с вызовом модалок

  В стилях модалка унаследовала свойство некликабельности, нужно потренироваться в отслеживании наследования стилей

  Не успел сделать кнопку копирования текста модалки в буфер обмена

Таск "Универсальный вход по email и телефону"

  Запутался с тем, как нужно передать payload  одним обьектом или несколькими, или несколькими , вложенными в один объект.
  Методом проб и ошибок определили передал одним

Таск "Испанская линия"
  
  Не догадался вставить костыль в экшены, а пытался решить проблему через HTTPactions , как Роман, но туда не получилось прокинуть данные из стейта
  В итоге разделил язык на сайте и язык запросов, но попытался это сделать во всех вызовах из вьюхи передачей другого языка. А нужно было сразу 
  лезть в экшены и там забирать из стейта язык для запросов.

  Я не  очень понимаю, почему мы во вьюхе передаем экшенам данные и хранящиеся в store  и не хранящиеся. 
  Хотя, по сути, в экшенах есть специальная функция getState(), из которой можно забирать эти переменные и потом не парится с отловом всех мест,
  где этот экшн вызывается.Возможно, если в разных местах вьюхи мы хотим передать разные данные в один экшн, так имеет смысл делать ,чтобы не плодить слишком
  много экшенов. Но у нас на сайте таким образом передается язык, который для всех общий и берется из state.

Таск "Time filter"

  Не догадался , что можно передать TimeFilter как children компонент в  общий для многих роутов компоент Back to line

  Использовал duration только для стран, а лиги и эвенты , при этом не фильтровались. После того, как эта проблема всплыла, пофиксил

  Ситуация с duration, который хранится в store и для всех запросов общий, не понимаю, зачем передавать его  в миллионе вызовов во вьюхе,
  если это можно сделать несколько раз для каждого запроса, где он нужен. А когда duration  отличается от общего уже передать его из вьюхи.

Таск "Back to line"

  Пока не разобрался с тем, почему функция context.router.goBack() иногда срабатывает правильно, а иногда пропускает предыдущее состояние роутера
  и прыгает на несколько шагов назад
